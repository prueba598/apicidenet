<?php

include 'bd/BD.php';

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD']=='GET'){
    if(isset($_GET['Emp_ID'])){
        $query="select * from Tbl_empleado where Emp_id=".$_GET['Emp_ID'];
        $resultado=metodoGet($query);
        echo json_encode($resultado->fetch(PDO::FETCH_ASSOC));
    }else if (isset($_GET['ideValidate']) && isset($_GET['tipValidate'])) {
        $query="select * from Tbl_empleado where Emp_NumIde=".$_GET['ideValidate']." and Emp_TipIde='".$_GET['tipValidate']."' and Emp_ID <> ".$_GET['ID']." ;";
        $resultado=metodoGet($query);
        echo json_encode($resultado->fetchAll()); 
    }else{
        $query="select * from Tbl_empleado";
        $resultado=metodoGet($query);
        echo json_encode($resultado->fetchAll()); 
    }
    header("HTTP/1.1 200 OK");
    exit();
}
if($_GET['METHOD']=='POST'){
    unset($_GET['METHOD']);

    $data = json_decode(file_get_contents("php://input"));
    $Emp_ApePri=$data->Emp_ApePri;
    $Emp_ApeSec=$data->Emp_ApeSec;
    $Emp_NomPri=$data->Emp_NomPri;
    $Emp_NomOtr=$data->Emp_NomOtr;
    $Emp_Pais=$data->Emp_Pais;
    $Emp_TipIde=$data->Emp_TipIde;
    $Emp_NumIde=$data->Emp_NumIde;
    $Emp_Are=$data->Emp_Are;
    $Emp_FecIng=$data->Emp_FecIng;
    $Emp_Ema=($data->Emp_NomPri).'.'.($data->Emp_ApePri);
    
    $query="insert into Tbl_empleado(Emp_Ema,Emp_ApePri,Emp_ApeSec,Emp_NomPri,Emp_NomOtr,Emp_Pais,Emp_TipIde,Emp_NumIde,Emp_Are,Emp_FecIng)
    SELECT CASE (COUNT(Emp_ID))
       WHEN 0 THEN '$Emp_Ema@CIDENET.COM.CO'
         ELSE
           CONCAT('$Emp_Ema','.',COUNT(Emp_ID),'@CIDENET.COM.CO')
       END AS 
    email, '$Emp_ApePri','$Emp_ApeSec','$Emp_NomPri','$Emp_NomOtr','$Emp_Pais','$Emp_TipIde','$Emp_NumIde','$Emp_Are','$Emp_FecIng'  
    FROM Tbl_empleado WHERE Emp_Ema LIKE '$Emp_Ema%'";
    $queryAutoIncrement="select MAX(Emp_ID) as Emp_ID from Tbl_empleado";
    $resultado=metodoPost($query, $queryAutoIncrement);
    echo json_encode($resultado);
    header("HTTP/1.1 200 OK");
    exit();
}

if($_GET['METHOD']=='PUT'){
    unset($_POST['METHOD']);

    $data = json_decode(file_get_contents("php://input"));
    $Emp_ID=$data->Emp_ID;
    $Emp_ApePri=$data->Emp_ApePri;
    $Emp_ApeSec=$data->Emp_ApeSec;
    $Emp_NomPri=$data->Emp_NomPri;
    $Emp_NomOtr=$data->Emp_NomOtr;
    $Emp_Pais=$data->Emp_Pais;
    $Emp_TipIde=$data->Emp_TipIde;
    $Emp_NumIde=$data->Emp_NumIde;
    $Emp_Are=$data->Emp_Are;
    $Emp_FecIng=$data->Emp_FecIng;
    $Emp_Ema=($data->Emp_NomPri).'.'.($data->Emp_ApePri);

    $query="UPDATE tbl_empleado SET Emp_ApePri='$Emp_ApePri',Emp_ApeSec='$Emp_ApeSec',Emp_NomPri='$Emp_NomPri',Emp_NomOtr='$Emp_NomOtr',Emp_Pais='$Emp_Pais',Emp_TipIde='$Emp_TipIde',Emp_NumIde='$Emp_NumIde',Emp_Are='$Emp_Are',Emp_FecIng='$Emp_FecIng',Emp_Ema=(SELECT CASE (COUNT(Emp_ID))
    WHEN 0 THEN '$Emp_Ema@CIDENET.COM.CO'
      ELSE
        CONCAT('$Emp_Ema','.',COUNT(Emp_ID),'@CIDENET.COM.CO')
    END AS 
 email FROM Tbl_empleado WHERE Emp_Ema LIKE '$Emp_Ema%') WHERE Emp_ID='$Emp_ID'";
    $resultado=metodoPut($query);
    echo json_encode($resultado);
    header("HTTP/1.1 200 OK");
    exit();
}

if($_POST['METHOD']=='DELETE'){
    unset($_POST['METHOD']);
    $Emp_ID=$_GET['Emp_ID'];
    $query="DELETE FROM Tbl_empleado WHERE Emp_id='$Emp_ID'";
    $resultado=metodoDelete($query);
    echo json_encode($resultado);
    header("HTTP/1.1 200 OK");
    exit();
}

header("HTTP/1.1 400 Bad Request");


?>
