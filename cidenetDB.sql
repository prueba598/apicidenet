-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-06-2022 a las 04:49:47
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cidenet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_empleado`
--

CREATE TABLE `tbl_empleado` (
  `Emp_ID` int(11) NOT NULL,
  `Emp_ApePri` varchar(20) NOT NULL,
  `Emp_ApeSec` varchar(20) NOT NULL,
  `Emp_NomPri` varchar(20) NOT NULL,
  `Emp_NomOtr` varchar(50) DEFAULT NULL,
  `Emp_Pais` varchar(56) NOT NULL,
  `Emp_TipIde` varchar(40) NOT NULL,
  `Emp_NumIde` varchar(20) NOT NULL,
  `Emp_Ema` varchar(50) DEFAULT NULL,
  `Emp_FecIng` date DEFAULT NULL,
  `Emp_Are` varchar(20) NOT NULL,
  `Emp_Est` varchar(6) NOT NULL DEFAULT 'Activo',
  `Emp_FecReg` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_empleado`
--

INSERT INTO `tbl_empleado` (`Emp_ID`, `Emp_ApePri`, `Emp_ApeSec`, `Emp_NomPri`, `Emp_NomOtr`, `Emp_Pais`, `Emp_TipIde`, `Emp_NumIde`, `Emp_Ema`, `Emp_FecIng`, `Emp_Are`, `Emp_Est`, `Emp_FecReg`) VALUES
(1, 'DUARTE', 'CUBILLOS', 'CRISTIAN', 'DAVID', 'Colombia', 'Cédula de Ciudadanía', '1069758316', 'CRISTIAN.DUARTE@CIDENET.COM.CO', '2022-06-13', 'Administración', 'Activo', '2022-06-18 17:19:19');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_empleado`
--
ALTER TABLE `tbl_empleado`
  ADD PRIMARY KEY (`Emp_ID`),
  ADD UNIQUE KEY `UNIQUE` (`Emp_NumIde`,`Emp_TipIde`),
  ADD UNIQUE KEY `UNIQUEEMA` (`Emp_Ema`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_empleado`
--
ALTER TABLE `tbl_empleado`
  MODIFY `Emp_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5974;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
